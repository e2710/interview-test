import { CellProps } from "../components/Cell";

// Shape type
type Shape = "circle" | "square" | "triangle";

// Color type
type Color = "red" | "green" | "blue";
type GameState = {
  cells: CellProps[];
  attempts: number;
  activeCellsIndex: number[];
  isFreezing: boolean
};
// your types here
export type { Shape, Color, GameState };
