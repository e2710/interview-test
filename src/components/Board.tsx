import React, { useState, useEffect } from "react";
import Cell from "./Cell";
import { CellProps } from "./Cell";
import "./Board.css";
import { Shape, Color, GameState } from "../types";
const colors: Color[] = ["red", "green", "blue"];
const shapes: Shape[] = ["circle", "square", "triangle"];

const Board: React.FC = () => {
  // states...
  const [gameState, setGameState] = useState<GameState>({
    cells: [],
    attempts: 0,
    activeCellsIndex: [],
    isFreezing: false,
  });
  useEffect(() => {
    // Initialize the game board with random shapes and colors
    generateCells();
  }, []);
  useEffect(() => {
    const revealArr = gameState.cells.filter((cell) => cell.isReveal).length;
    if (revealArr === 16) {
      alert(`You win the game with ${gameState.attempts} attempts`);
    }
  }, [gameState]);

  function generateCells() {
    const originalCells: CellProps[] = [];

    while (originalCells.length < 8) {
      const newCell: CellProps = {
        shape: shapes[Math.floor(Math.random() * shapes.length)],
        color: colors[Math.floor(Math.random() * colors.length)],
        isReveal: false,
        isFlipped: false,
      };
      if (
        !originalCells.some((cell) => {
          return cell.shape === newCell.shape && cell.color === newCell.color;
        })
      ) {
        originalCells.push(newCell);
      }
    }
    const doubledCells: CellProps[] = [
      ...originalCells,
      ...JSON.parse(JSON.stringify(originalCells)),
    ];

    doubledCells.sort(() => Math.random() - 0.5);
    setGameState((currentGameState) => {
      return { ...currentGameState, cells: doubledCells };
    });
  }

  const handleCellClick = (index: number) => {
    // Reveal cell, check for matches, update game state, and handle game completion
    const modifyCells = [...gameState.cells];

    modifyCells[index] = {
      ...modifyCells[index],
      isFlipped: true,
    };
    setGameState((currentGameState) => {
      const newGameState = {
        ...currentGameState,
        cells: modifyCells,
        activeCellsIndex: [...currentGameState.activeCellsIndex, index],
      };
      checkMatch(newGameState);
      return newGameState;
    });
  };
  function checkMatch(newGameState: GameState) {
    const modifyCells = [...newGameState.cells];
    const flippingCellsIndex = [...newGameState.activeCellsIndex];

    if (flippingCellsIndex.length < 2) {
      return;
    }
    const [firstCellIndex, secondCellIndex] = flippingCellsIndex;
    const [firstCell, secondCell] = [
      modifyCells[firstCellIndex],
      modifyCells[secondCellIndex],
    ];
    const isMatch =
      firstCell.color === secondCell.color &&
      firstCell.shape === secondCell.shape;

    if (isMatch) {
      flippingCellsIndex.forEach((index) => {
        modifyCells[index] = {
          ...modifyCells[index],
          isReveal: true,
        };
      });
      setGameState((currentGameState) => {
        return {
          ...currentGameState,
          cells: modifyCells,
          activeCellsIndex: [],
          attempts: currentGameState.attempts + 1,
        };
      });
      return;
    }
    // Flip cells back when not match

    setGameState((currentGameState) => {
      return {
        ...currentGameState,
        attempts: currentGameState.attempts + 1,
        isFreezing: true,
      };
    });
    setTimeout(() => {
      flippingCellsIndex.forEach((index) => {
        modifyCells[index] = {
          ...modifyCells[index],
          isFlipped: false,
        };
      });

      setGameState((currentGameState) => {
        return {
          ...currentGameState,
          cells: modifyCells,
          activeCellsIndex: [],
          isFreezing: false,
        };
      });
    }, 1000);
  }

  return (
    <>
      <div
        className={`board-container ${gameState.isFreezing ? "freeze" : ""}`}
      >
        <div className="board">
          {
            /* Render each cell in the board */
            gameState.cells.map((cell, index) => {
              return (
                <Cell
                  key={index}
                  {...cell}
                  index={index}
                  flipCell={handleCellClick}
                />
              );
            })
          }
        </div>
      </div>
      <p>Attempts: {gameState.attempts}</p>
    </>
  );
};

export default Board;
