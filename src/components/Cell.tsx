import React from "react";
import "./Cell.scss";
import { Shape, Color } from "../types";
export interface CellProps {
  // Your code here
  shape: Shape;
  color: Color;
  isReveal: boolean;
  isFlipped: boolean;
  flipCell?: (index: number) => void;
  index?: any;
}

const Cell: React.FC<CellProps> = (props: CellProps) => {
  return (
    <>
      <div
        className={`cell-container ${
          props.isReveal ? "cell-reveal-flipped" : ""
        }`}
        onClick={() => props.flipCell && props.flipCell(props.index)}
      >
        {props.isFlipped || props.isReveal ? (
          <div className={`cell ${props.shape} ${props.color}`}></div>
        ) : (
          <div className="cell cell-not-flipped"></div>
        )}
      </div>
    </>
  );
};

export default Cell;
